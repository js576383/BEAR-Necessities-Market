# Template Use: 
* *Text within italics describes the purpose of each section and should be deleted*

## Pre-checklist

- [ ] I have searched for, and did not find, an existing issue at the [top-level](https://gitlab.com/LibreFoodPantry).

## Stories

*Typically only a single story is necessary. If two or more roles want a feature with variations in behavior, then consider more than one story.*

Story 1: As a [role] I want [feature] so that [benefit].

Story 2: As a [role] I want [feature] so that [benefit].

## Ready checklist

This story must have the following characteristics before being considered ready for work. After this section, fill in as much as possible. 

- [ ] Independent (of other issues in flow)
- [ ] Negotiable (and negotiated)
- [ ] Valuable (the value to an identified role has been identified)
- [ ] Estimable (the size of this story has been estimated)
- [ ] Small (can be completed in 50% or less of a single iteration)
- [ ] Testable (has testable acceptance criteria)

Also

- [ ] The roles that benefit from this issue are labeled (e.g., `role:*`).
- [ ] The related activity in the story map has been identified (e.g., `activity:*`). This should be an activity in the Story Map. 

## Diagrams

*Include screenshots, mock-ups, design diagrams and any other images that help define the activity* 

## Acceptance Criteria

Scenario 1 | TITLE
-----------|-----
     GIVEN | Given 1
     GIVEN | Given 2
     GIVEN | Given 3
      WHEN | Action
      THEN | Result 1
      THEN | Result 2

Scenario 2 | TITLE
-----------|-----
     GIVEN | Given 1
      WHEN | Action
      THEN | Result 1
      THEN | Result 2
      THEN | Result 3

## Related issues

Links to issues related to this issue, and how they are related.

- Parent: issue-url
- Task: issue-url
- Blocked-by: issue-url
...

## Estimate

*Estimate of time required to complete the story. Add the "Weight" on the right-hand side of the screen when creating a new issue. Use the Fibonacci sequence as a scale:*
- *0 = Quick less than an hour worth of work*
- *1 = Half-day worth of work*
- *2 = One day worth of work*
- *3 = Less than a sprint, more than a day worth of work*
- *5 = Full sprint worth of work*
- *8 = Multiple sprints worth of work*
- *13 = Many sprints worth of work*
- *...*

## Version 1.0 
*This is the version of the template being used as templates may change over time.*

/label type::user
